package demo2;

import java.util.Random;
import java.util.Scanner;
import java.util.stream.IntStream;

public class ClientSide
{
    public static void main(String[] args){
        Scanner reader = new Scanner(System.in);
  
        TwoMAStrategy ai = new TwoMAStrategy();
        Random random = new Random();
        //Random list of stock prices; to be divided by 10 to convert to double
        IntStream inStream = random.ints(1, 400000);
        
        System.out.println ("Welcome to Skynet 1.0, here to take over the... I mean I am an AI stock prices tracker");
        System.out.println ("For any technical errors or otherwise please contact Dan Deng personally");
        
        try{
            System.out.println ("How many days would you like to track?");
            int lim = reader.nextInt ();
            System.out.println ("The period of long prices you would like to track:");
            int min_long = reader.nextInt ();
            System.out.println ("The period of short prices you would like to track:");
            int min_short = reader.nextInt ();
            
            if (min_long <=0 || min_short <=0){
                System.out.println ("Inputs must be non-zero");
                throw new Exception();
            }
            int[] array = inStream.limit(lim).toArray();
            int min_long_period = min_long;
            int min_short_period = min_short;
            ai.setShortPrices (min_short_period);
            ai.setLongPrices (min_long_period);
            
            int counter = 0;
            for(int i:array){
                counter ++;
                String tag = "";
                String decision = "";
                double tick = i/10.0;
                double rand = Math.random();
                if(rand > 0.5){
                    tag = "short";
                }
                else{
                    tag = "long";
                }
                decision = ai.track (tick, tag);
                System.out.println ("At current time " + counter + " the decision is to: " + decision);
            }
        }catch(Exception e){
            System.out.println ("Inputs must be a number");
        

       
        }
        
        
        
    }
}
