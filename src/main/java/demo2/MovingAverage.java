package demo2;

import java.util.*;


public class MovingAverage
{
    int period;
    Queue<Double> dataStorage = new LinkedList<Double>();
    
    public MovingAverage(int initialPeriod){
        period = initialPeriod;
    }
    
    public MovingAverage(Queue<Double> initialData, int initialPeriod) {
        dataStorage = initialData;
        period = initialPeriod;
        
    }
    
    public double addData(double data) {
        if (dataStorage.size() < period) {
            dataStorage.add (data);
            
            if (dataStorage.size () >= period) {
                return calcMA();
            }
            else {
                return -1;
            }
        }
        else {
            dataStorage.remove();
            dataStorage.add (data);
            return calcMA();
        }
    }
    
    public double calcMA() {
        try {
            Double count = (double) 0;
            for (int i=0; i<period; i++) {
                count += ((LinkedList<Double>) dataStorage).get(i);
                //System.out.println (count);
            }
            //System.out.println ();
            //System.out.println ((Math.round((count/period) * 10.0)/10.0));
            return Math.round((count/period) * 10.0)/10.0;
        }
        catch (Exception e) {
            System.out.println("Average has been asked too soon");
            return -1;
        }
    }
    
   /* public static void main(String[] args) {
        Queue<Double> sampleData = new LinkedList<Double>();
        sampleData.add(2.0);
        sampleData.add(4.7);
        sampleData.add(2.9);
        sampleData.add(1.1);
        sampleData.add(0.8);
        sampleData.add(4.4);
        MovingAverage test = new MovingAverage(sampleData, 6);
        System.out.println(test.calcMA());
    }
    */
}
