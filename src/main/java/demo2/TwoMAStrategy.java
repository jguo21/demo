package demo2;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class TwoMAStrategy
{
    static MovingAverage short_prices;
    static MovingAverage long_prices;
    static double long_average;
    static double short_average;
    // 0 = neutral, 1=short, 2=long
    static int flag;
    
    //Empty constructor to reset flag
    public TwoMAStrategy(){
        flag = 0;
    }
    
    //Set short prices with initial list of prices and period
    public static void setShortPrices(Queue<Double> ini, int period){
        short_prices = new MovingAverage(ini, period); 
        short_average = short_prices.calcMA ();
    }
    
    //Set short prices with no initial list of prices
    public static void setShortPrices(int period){
        short_prices = new MovingAverage(period); 
    }
    
    public static void setLongPrices(Queue<Double> ini, int period){
        long_prices = new MovingAverage(ini, period);
        long_average = long_prices.calcMA ();
    }
    public static void setLongPrices(int period){
        long_prices = new MovingAverage(period);
    }
    
    //Tracks and updates long and short prices with incoming tag and ticker
    public static String track(double t, String tag){
        try{
            if(tag.equals ("long")){
                long_average = long_prices.addData(t);
            }else if(tag.equals ("short")){
                short_average = short_prices.addData(t);
            }else{
                return "Incorrect Tag";
            }
            
            //Make decision based on current averages
            return make_decision();
        }catch(Exception e){
            return "Well someone goof'd";
        }
    }
    
    //Logic for making decisions
    public static String make_decision(){
        if(short_average  < 0 || long_average < 0 ){        
        return "Not enough prices";}
        
        //Initial. If short prices > long prices buy and set flag, vice versa
        if(flag==0){
            if(short_average > long_average){
                flag = 1;
                return "Buy";
            }
            else{
                flag = 2;
                return "Sell";
            }
        }else{
            //Check which average is greater, check status of flag and update/return respectively
            if(long_average > short_average){
                if(flag == 2){
                    return "Do nothing";
                }else{
                    flag = 2;
                    return "Sell";
                }
         
            }
            else{
                if(flag == 1){
                    return "Do nothing";
                    
                }
                else{
                    flag = 1;
                    return "Buy";
                }
            }
            
        }
    }
}
