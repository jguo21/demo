package demo2;

import static org.junit.Assert.*;

import java.util.*;
import org.junit.Before;
import org.junit.Test;


public class MovingAverageTest
{
    Queue<Double> sampleData = new LinkedList<Double>();
    
    @Before
    public void setUpMockData() {
        sampleData.add(2.0);
        sampleData.add(4.7);
        sampleData.add(2.9);
        sampleData.add(1.1);
        sampleData.add(0.8);
        sampleData.add(4.4);
    }

    
    @Test 
    public void testAddData() {
        MovingAverage test = new MovingAverage(sampleData, 6);
        test.addData (12.3);
        /*
        System.out.println("====");
        for(double i : sampleData){
            System.out.println (i);
        }
        System.out.println("====");*/
        assertEquals(6, test.dataStorage.size());
    }
    
    
    @Test
    public void testcalcMA() {
        MovingAverage test = new MovingAverage(sampleData, 6);
        test.addData (12.3);
        System.out.println ("test" + test.calcMA());
        assertEquals(4.4, test.calcMA(), 0.01);
    }
}



