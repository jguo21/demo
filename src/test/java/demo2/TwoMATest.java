package demo2;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import org.junit.Before;
import org.junit.Test;



public class TwoMATest
{
    Queue<Double> testLongPrices = new LinkedList<Double>();
    Queue<Double> testShortPrices = new LinkedList<Double>();
    int longPeriod;
    int shortPeriod;
    
    @Before
    public void initiate(){
        testLongPrices.add (1.0);
        testLongPrices.add (2.0);
        testLongPrices.add (3.0);
        
        testShortPrices.add (10.0);
        testShortPrices.add(20.0);
        testShortPrices.add (30.0);
        
        longPeriod = 3;
        shortPeriod = 3;
    }
    
    @Test 
    public void testBuy(){
        //Short average = 20
        //Long average = 2
        TwoMAStrategy ai = new TwoMAStrategy();
        ai.setLongPrices (testLongPrices, longPeriod);
        ai.setShortPrices (testShortPrices, shortPeriod);
        
        assertEquals("Buy", ai.make_decision ());
        
    }
    
    @Test
    public void testSell(){
        //Short average = 20
        //Long average = 168.3
       
        TwoMAStrategy ai = new TwoMAStrategy();
        ai.setLongPrices (testLongPrices, longPeriod);
        ai.setShortPrices (testShortPrices, shortPeriod);
        
        
        assertEquals("Sell", ai.track (500.0, "long"));
    }
    
    @Test
    public void testDoNothing(){
        TwoMAStrategy ai = new TwoMAStrategy();
        ai.setLongPrices (testLongPrices, longPeriod);
        ai.setShortPrices (testShortPrices, shortPeriod);
        ai.track (500.0, "short");

        assertEquals("Do nothing", ai.track (600.0, "short"));
    }
    
    @Test
    public void crossOver(){
        //System.out.println ("===Cross Over Test===");
        TwoMAStrategy ai = new TwoMAStrategy();
        ai.setLongPrices (testLongPrices, longPeriod);
        ai.setShortPrices (testShortPrices, shortPeriod);

        assertEquals("Sell", ai.track (500.0, "long"));
        assertEquals("Do nothing", ai.track (6.0, "short"));
        assertEquals("Buy", ai.track (5000.0, "short"));
        assertEquals("Sell", ai.track (500000.0, "long"));
    }
    
    @Test
    public void iniWithNoData(){
        TwoMAStrategy ai = new TwoMAStrategy();
        ai.setLongPrices (longPeriod);
        ai.setShortPrices (shortPeriod);
        assertEquals("Not enough prices", ai.track (1.0, "long"));
        assertEquals("Not enough prices", ai.track (10.0, "short"));
        assertEquals("Not enough prices", ai.track (20.0, "short"));
        assertEquals("Not enough prices", ai.track (2.0, "long"));
        assertEquals("Not enough prices", ai.track (3.0, "long"));
        
        assertEquals("Buy", ai.track (30.0, "short"));
        assertEquals("Do nothing", ai.track (5000.0, "short"));
        assertEquals("Sell", ai.track (500000.0, "long"));
    }
}
